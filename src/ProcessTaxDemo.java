import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Function;

public class ProcessTaxDemo {

    static Predicate<String> kaTaxPredicate = (state) -> state.equalsIgnoreCase("KA");
    static Predicate<String> klTaxPredicate = (state) -> state.equalsIgnoreCase("KL");
    static Predicate<String> otherTaxPredicate = (state) -> kaTaxPredicate.or(klTaxPredicate).negate().test(state);

    static Function<Double, Double> kaTaxFunction = (amount) -> amount + 18/100 * amount;
    static Function<Double, Double> klTaxFunction = (amount) -> amount + 15/100 * amount;
    static  Function<Double, Double> otherStateTaxFunction = (amount) -> amount + 25/100 * amount;



    static Map<Predicate<String>, Function<Double,Double>> stateToTax = new HashMap<>();

    public static void main(String[] args) {
       // processTax(25000, "KA");
        stateToTax.put(kaTaxPredicate, kaTaxFunction);
        stateToTax.put(klTaxPredicate, klTaxFunction);
        stateToTax.put(otherTaxPredicate, otherStateTaxFunction);
    }

    public static double processTax(double amount, String state){
        if(state.equalsIgnoreCase("KA")){
            return amount + 18/100 * amount;
        } else if(state.equalsIgnoreCase("KL")){
            return amount + 15/100 * amount;
        } else {
            return amount + 20/100 * amount;
        }
    }

    public static double processTax(double amount, String state, Map<Predicate<String>, Function<Double,Double>> stateToTaxMap){
        Set<Map.Entry<Predicate<String>, Function<Double, Double>>> entries = stateToTaxMap.entrySet();
        Iterator<Map.Entry<Predicate<String>, Function<Double, Double>>> iterator = entries.iterator();
        while (iterator.hasNext()){
            Map.Entry<Predicate<String>, Function<Double, Double>> entry = iterator.next();
            Predicate<String> statePredicate = entry.getKey();
            if(statePredicate.test(state)){
                Function<Double, Double> function = entry.getValue();
                Double totalTaxAmount = function.apply(amount);
                return totalTaxAmount;
            }
        }
        return 0;
    }
}