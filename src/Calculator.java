public class Calculator {

    /*public void sum(int a ,int b){

    }
    public void subtract(int a ,int b){

    }
    public void multiply(int a ,int b){

    }
    public void divide(int a ,int b){

    }*/

    public int apply(int a, int b, Function function){
       return function.compute(a,b);
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.apply(23, 45, new SubtractFunction());
    }
}

interface Function {
    int compute(int a, int b);
}

class ModFunction implements Function {

    @Override
    public int compute(int a, int b) {
        return a % b;
    }
}

class SubtractFunction implements Function {

    @Override
    public int compute(int a, int b) {
        return a - b;
    }
}
class MaxFunction implements Function {

    @Override
    public int compute(int a, int b) {

        return a  > b ? a : b;
    }
}

class MinFunction implements Function {

    @Override
    public int compute(int a, int b) {

        return a  < b ? a : b;
    }
}