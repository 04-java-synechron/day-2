import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;

/*@FunctionalInterface
interface Predicate<T> {
    boolean test(T t);
}*/

class ConditionDivisibleBy11 implements Predicate<Integer> {

    @Override
    public boolean test(Integer integer) {
        return integer % 11 == 0;
    }
}
class ConditionDivisibleBy12 implements Predicate<Integer>{

    @Override
    public boolean test(Integer integer) {
        return integer % 12 == 0;
    }
}

public class DeclarativeProgrammingDemo {

    public static void main(String[] args) {
        int array[] = {12,32,11,23,45,655,32,13,14,22,55};
        List<Integer> divisibleBy11 = new ArrayList<>();
        // problem statement - identify the numbers which are divisible by 11
        //step-1
        for (int i = 0; i < array.length; i++) {
            int number = array[i];
            if (number % 11 == 0 && number < 50 && (number != 11 | number != 22)){
                divisibleBy11.add(number);
            }
        }

        //select * from array where number < 50 and number not (11,22) and number mod 11 == 0 order by number desc

        //design patterns - separate moving parts from the fixed part
        //variable assignment
        /*
             int value = 676;
             User user = new User();
             Predicate<String> predicate = functional behaviou
             predicate.test(23);
         */

        List<String> str = new ArrayList<>();
        Predicate<Integer> conditionDivisibleBy11 = value ->  value % 11 == 0;

        Predicate<Integer> conditionLessThan50 = (value) -> value < 50;

        Predicate<Integer> conditionNotEquals11 = integer -> integer != 11;

        Predicate<Integer> conditionNotEquals22 = (integer) ->  integer != 22;

        //boolean returnValue = conditionDivisibleBy11.test(12);

        Predicate<Integer> consolidatedPredicate = conditionDivisibleBy11.and(conditionLessThan50).and(conditionNotEquals11.or(conditionNotEquals22));

        processData(array, divisibleBy11, consolidatedPredicate);

        //System.out.println("Return value "+ returnValue);

        BiFunction<Integer[], Predicate<Integer>, List<Integer>> processDataWIthDeclaratively = (data, predicate) -> {
            List<Integer> output = new ArrayList<>();
            for (int i = 0; i < data.length; i++) {
                int number = data[i];
                if (predicate.test(number)){
                    output.add(number);
                }
            }
            return output;
        };
    }

    public static void processData(int[] array, List<Integer> output, Predicate<Integer> condition){
        for (int i = 0; i < array.length; i++) {
            int number = array[i];
            if (condition.test(number)){
                output.add(number);
            }
        }
    }
}