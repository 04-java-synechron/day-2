
import jdk.jshell.spi.ExecutionControlProvider;

import java.io.FileNotFoundException;
import java.net.BindException;
import java.net.ConnectException;

abstract class Parent<T extends Number> {

     public Object baseMethod(T arg) throws BindException, ConnectException, ArrayIndexOutOfBoundsException {
        System.out.println("This method might throw BindException or ConnectException");
        return null;
    }


}

class Child<T> extends Parent<Integer>{

    @Override
    public Number baseMethod(Integer arg) {
        System.out.println("This method might throw BindException or ConnectException");
        return 0;
    }
}

public class ExceptionHandlingDemo {

    public static void main(String[] args) {
       Child child = new Child();
       child.baseMethod(34);
       //child.baseMethod(34585.25F);
    }
}