import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class FunctionDemo {

    public static void main(String[] args) {
        Function<String, Integer> mapStringToInt = val -> val.length();

        Product iPhone = new Product("iPHone-11", ProductType.APPLE, 55_000);

        Function<Product, String> mapProductToName = (product) -> product.getName();

        Function<User, String > fetchAddressFromUser = (user) -> user.getAddress().getCity();

        BiFunction<String, User, String> mapUserAddressByName = (name, user) ->name + ":: belongs to:: " +user.getAddress().getState();

        String result = mapUserAddressByName.apply("Rashid", new User("Rashid", new Address("Bangalore", "Karnataka", "577142")));
        System.out.println(result);

        Consumer<Product> printProduct = product -> System.out.println(product);
        printProduct.accept(iPhone);


    }

    public Product getProduct () {
        Supplier<Product> productSupplier =  () -> new Product("iPhone", ProductType.APPLE, 25000);
        return productSupplier.get();
    }
}

class User {
    private String name;
    private Address address;

    public User(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}

class Address {
    private String city;
    private String state;
    private String zipCode;


    public Address(String city, String state, String zipCode) {
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}