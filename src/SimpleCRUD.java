import java.util.Optional;
import java.util.Set;

public interface SimpleCRUD<T extends Entity<?>> {

     T save(T t);

     Set<T> findAll();

     Optional<T> findById(Long id);

     void deleteById(Long Id);
}