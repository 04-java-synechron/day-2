import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import static java.util.List.of;

public class ProductFilter {

    public static void main(String[] args) {
        Product iPhone = new Product("iPHone-11", ProductType.APPLE, 55_000);
        Product macBookPro = new Product("macBook-Pro", ProductType.APPLE, 155_000);
        Product airPod = new Product("airPod", ProductType.APPLE, 5_000);
        Product oppoF19Pro = new Product("Oppo-F19Pro", ProductType.OPPO, 25_000);
        Product galaxyS12 = new Product("Samsung-Galaxy-S12", ProductType.SAMSUNG, 65_000);

        Predicate<Product> isIPhone = product -> product.getName().equalsIgnoreCase("iPHone-11");
        Predicate<Product> isMacBookPro = product -> product.getName().equalsIgnoreCase("macBook-Pro");
        Predicate<Product> isAirPod = product -> product.getName().equalsIgnoreCase("airpod");

        Predicate<Product> isAppleProduct = isAirPod.or(isIPhone).or(isMacBookPro);

        Predicate<Product> isNotAppleProduct = isAppleProduct.negate();

        Predicate<Product> lessThan50K = product -> product.getPrice() < 50000;

        List<Product> productList = new ArrayList<>();
        productList.add(iPhone);
        productList.add(macBookPro);
        productList.add(airPod);
        productList.add(oppoF19Pro);
        productList.add(galaxyS12);

        List<Product> products = Arrays.asList(iPhone,macBookPro, airPod,oppoF19Pro,galaxyS12);

        List<Product> allProducts = of(iPhone, macBookPro, airPod, oppoF19Pro, galaxyS12);
        List<Product> appleProducts = new ArrayList<>();
        for(Product product: products){
            if(isAppleProduct.test(product)){
                appleProducts.add(product);
            }
        }
        for(Product appleProduct: appleProducts){
            System.out.println(appleProduct);
        }
    }
}