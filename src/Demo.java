public class Demo {

    private void nonStaticMethod(){
        System.out.println("Non static method");
    }
    private static void staticMethod(){
        System.out.println("Non static method");
    }

    class NonStaticClass {
        public void test(){

        }
    }

    static class StaticClass {
        public static void test(){

        }
    }

    public static void main(String[] args) {
        Demo.StaticClass.test();
        new Demo().new NonStaticClass().test();
    }
}