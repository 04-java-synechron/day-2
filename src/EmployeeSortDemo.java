import java.util.Comparator;

public class EmployeeSortDemo {

    public static void main(String[] args) {
        Comparator<Employee> empSortBySalaryAsc = new Comparator<Employee>() {
            @Override
            public int compare(Employee employee1, Employee employee2) {
                return (int)(employee1.getSalary() - employee2.getSalary());
            }
        };

        Comparator<Employee> empSortBySalaryDesc = new Comparator<Employee>() {
            @Override
            public int compare(Employee employee1, Employee employee2) {
                return (int)(employee2.getSalary() - employee1.getSalary());
            }
        };
    }
}