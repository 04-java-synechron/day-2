import java.util.Date;

public class Order extends Entity<Long> {

    private String customerName;

    private String emailAddress;

    private Date dob;

    public Order(OrderBuilder orderBuilder){
        this.customerName = orderBuilder.customerName;
        this.emailAddress = orderBuilder.emailAddress;
        this.dob = orderBuilder.dob;
    }

    //static member of class

    public static class OrderBuilder {

        private String customerName;

        private String emailAddress;

        private Date dob;

        public OrderBuilder(String customerName, String emailAddress){
            this.customerName = customerName;
            this.emailAddress = emailAddress;
        }

        public  OrderBuilder dob(Date orderDate){
            dob = orderDate;
            return this;
        }

        public Order build(){
            return new Order(this);
        }
    }
}