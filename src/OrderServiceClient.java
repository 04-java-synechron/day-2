import java.util.Date;

public class OrderServiceClient {
    public static void main(String[] args) {
        Order order = new Order.OrderBuilder("Ramesh", "ramesh@gmail.com")
                            .dob(new Date())
                            .build();
        OrderDAOImpl orderDAO = new OrderDAOImpl();
        orderDAO.save(order);

        //imperative style
        for(int index = 0; index < 10; index ++){
            //process
        }
        //
        int array[] = {11,22,33,44, 54 , 43 , 23};

        for(int index : array ) {

        }



    }
}