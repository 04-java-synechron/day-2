import java.util.Date;

public class Payment extends Entity<Long> {

    private double amount;
    private Date transactionDate;
    private static Payment payment;

    private Payment(double amount, Date transactionDate) {
        this.amount = amount;
        this.transactionDate = transactionDate;
    }

    public static Payment getInstance(double amount, Date transactionDate){
        if (payment == null){
            payment = new Payment(amount, transactionDate);
        }
        return payment;
    }
}